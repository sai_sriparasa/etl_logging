# README #

This repo carries the sql scripts to create the required tables and functions for ETL Logging with Pentaho.

For more information about this repo, please use this confluence page [https://newsinc.atlassian.net/wiki/display/DATA/ETL+Logging+DB]