
CREATE OR REPLACE FUNCTION dw.create_batch (in jobtypeid int4) RETURNS int4 AS
'
declare
    batchId integer;
begin
    
    insert into dw.batch(status_id, job_type_id) values(1,jobTypeId);
    SELECT currval(pg_get_serial_sequence(''dw.batch'',''id'')) into batchId;
    
    return batchId;
end;
'
LANGUAGE 'plpgsql'
GO
