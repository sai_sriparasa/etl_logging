DROP FUNCTION IF EXISTS dw.create_step(integer,integer);

CREATE OR REPLACE FUNCTION dw.create_step (in batchid int4, in steptypeid int4, in linesread int4 default NULL, in lineswritten int4 default NULL, in linesupdated int4 default NULL, 
in linesinput int4 default NULL, in linesoutput int4 default NULL, in  linesrejected int4 default NULL, in  stepname varchar(100) default NULL) RETURNS int4 AS
'
declare
    stepId integer;
begin
    
    insert into dw.step(status_id, batch_id, step_type_id,lines_read, lines_written, lines_updated, lines_input, lines_output, lines_rejected, step_name) values(1,batchId, stepTypeId,linesRead, linesWritten, linesUpdated, linesInput, linesOutput, linesRejected, stepName);
    SELECT currval(pg_get_serial_sequence(''dw.step'',''id'')) into stepId;
    
    return stepId;
end;
'
LANGUAGE 'plpgsql'
GO