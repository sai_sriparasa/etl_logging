
CREATE OR REPLACE FUNCTION dw.end_batch (in batchid int4) RETURNS bool AS
'
begin
    
    update dw.batch set status_id = 3, updated=current_timestamp where id = batchId;
    
    if FOUND then
        return TRUE;
    else
        return FALSE;
    end if;

end;
'
LANGUAGE 'plpgsql'
GO
