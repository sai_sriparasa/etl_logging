
CREATE OR REPLACE FUNCTION dw.end_step (in stepid int4) RETURNS bool AS
'
begin
    
    update dw.step set status_id = 3, updated=current_timestamp where id = stepId;
    
    if FOUND then
        return TRUE;
    else
        return FALSE;
    end if;

end;
'
LANGUAGE 'plpgsql'
GO
