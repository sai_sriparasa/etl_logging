
CREATE OR REPLACE FUNCTION dw.update_batch_status (in batchid int4, in statusid int4) RETURNS bool AS
'
begin
    update dw.batch set status_id = statusId, updated=current_timestamp where id = batchId;
    
    if FOUND then
        return TRUE;
    else
        return FALSE;
    end if;

end;
'
LANGUAGE 'plpgsql'
GO
