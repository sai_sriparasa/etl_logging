
CREATE OR REPLACE FUNCTION dw.update_step_status (in stepid int4, in statusid int4) RETURNS bool AS
'
begin
    update dw.step set status_id = statusId, updated = current_timestamp where id = stepId;
    
    if FOUND then
        return TRUE;
    else
        return FALSE;
    end if;

end;
'
LANGUAGE 'plpgsql'
GO
