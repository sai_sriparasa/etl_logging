
CREATE OR REPLACE FUNCTION dw.log_error (in batchid int4, in stepid int4, in message varchar) RETURNS int4 AS
'
declare
    errorId integer;
begin
    insert into dw.error(batch_id, step_id, message) values(batchId, stepId,message);
    SELECT currval(pg_get_serial_sequence(''dw.error'',''id'')) into errorId;
    
    return errorId;
end;
'
LANGUAGE 'plpgsql'
GO
