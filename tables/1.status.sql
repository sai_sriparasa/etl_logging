CREATE TABLE dw.status  ( 
	id         	serial NOT NULL,
	description	varchar(25) NULL,
	comment    	text NULL,
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.status TO diuser WITH GRANT OPTION
GO
GRANT SELECT(description), INSERT(description), UPDATE(description), REFERENCES(description) ON dw.status TO diuser WITH GRANT OPTION
GO
GRANT SELECT(comment), INSERT(comment), UPDATE(comment), REFERENCES(comment) ON dw.status TO diuser WITH GRANT OPTION
GO
