CREATE TABLE dw.channel  ( 
	id_batch            	int4 NULL,
	channel_id          	varchar(255) NULL,
	log_date            	timestamp NULL,
	logging_object_type 	varchar(255) NULL,
	object_name         	varchar(255) NULL,
	object_copy         	varchar(255) NULL,
	repository_directory	varchar(255) NULL,
	filename            	varchar(255) NULL,
	object_id           	varchar(255) NULL,
	object_revision     	varchar(255) NULL,
	parent_channel_id   	varchar(255) NULL,
	root_channel_id     	varchar(255) NULL 
	)
WITHOUT OIDS 
TABLESPACE pg_default
GO
GRANT SELECT(root_channel_id), INSERT(root_channel_id), UPDATE(root_channel_id), REFERENCES(root_channel_id) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(repository_directory), INSERT(repository_directory), UPDATE(repository_directory), REFERENCES(repository_directory) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(parent_channel_id), INSERT(parent_channel_id), UPDATE(parent_channel_id), REFERENCES(parent_channel_id) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(object_revision), INSERT(object_revision), UPDATE(object_revision), REFERENCES(object_revision) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(object_name), INSERT(object_name), UPDATE(object_name), REFERENCES(object_name) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(object_id), INSERT(object_id), UPDATE(object_id), REFERENCES(object_id) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(object_copy), INSERT(object_copy), UPDATE(object_copy), REFERENCES(object_copy) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(logging_object_type), INSERT(logging_object_type), UPDATE(logging_object_type), REFERENCES(logging_object_type) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(log_date), INSERT(log_date), UPDATE(log_date), REFERENCES(log_date) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id_batch), INSERT(id_batch), UPDATE(id_batch), REFERENCES(id_batch) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(filename), INSERT(filename), UPDATE(filename), REFERENCES(filename) ON dw.channel TO diuser WITH GRANT OPTION
GO
GRANT SELECT(channel_id), INSERT(channel_id), UPDATE(channel_id), REFERENCES(channel_id) ON dw.channel TO diuser WITH GRANT OPTION
GO
