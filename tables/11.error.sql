CREATE TABLE dw.error  ( 
	id      	serial NOT NULL,
	message 	varchar(100) NULL,
	batch_id	int4 NULL,
	step_id 	int4 NULL,
	created 	timestamp NOT NULL DEFAULT now(),
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
ALTER TABLE dw.error
	ADD CONSTRAINT step-error
	FOREIGN KEY(step_id)
	REFERENCES dw.step(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
ALTER TABLE dw.error
	ADD CONSTRAINT batch-error
	FOREIGN KEY(batch_id)
	REFERENCES dw.batch(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
GRANT SELECT(step_id), INSERT(step_id), UPDATE(step_id), REFERENCES(step_id) ON dw.error TO diuser WITH GRANT OPTION
GO
GRANT SELECT(message), INSERT(message), UPDATE(message), REFERENCES(message) ON dw.error TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.error TO diuser WITH GRANT OPTION
GO
GRANT SELECT(created), INSERT(created), UPDATE(created), REFERENCES(created) ON dw.error TO diuser WITH GRANT OPTION
GO
GRANT SELECT(batch_id), INSERT(batch_id), UPDATE(batch_id), REFERENCES(batch_id) ON dw.error TO diuser WITH GRANT OPTION
GO
