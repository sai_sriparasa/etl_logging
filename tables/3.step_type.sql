CREATE TABLE dw.step_type  ( 
	id         	serial NOT NULL,
	description	varchar(25) NULL,
	job_type_id	int4 NULL,
	comment    	text NULL,
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
ALTER TABLE dw.step_type
	ADD CONSTRAINT job type-step type
	FOREIGN KEY(job_type_id)
	REFERENCES dw.job_type(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
GRANT SELECT(job_type_id), INSERT(job_type_id), UPDATE(job_type_id), REFERENCES(job_type_id) ON dw.step_type TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.step_type TO diuser WITH GRANT OPTION
GO
GRANT SELECT(description), INSERT(description), UPDATE(description), REFERENCES(description) ON dw.step_type TO diuser WITH GRANT OPTION
GO
GRANT SELECT(comment), INSERT(comment), UPDATE(comment), REFERENCES(comment) ON dw.step_type TO diuser WITH GRANT OPTION
GO
