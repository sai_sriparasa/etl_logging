CREATE TABLE dw.batch  ( 
	id         	serial NOT NULL,
	status_id  	int4 NULL,
	created    	timestamp NOT NULL DEFAULT now(),
	updated    	timestamp NULL,
	job_type_id	int4 NULL,
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
ALTER TABLE dw.batch
	ADD CONSTRAINT job-status
	FOREIGN KEY(status_id)
	REFERENCES dw.status(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
ALTER TABLE dw.batch
	ADD CONSTRAINT batch - job type
	FOREIGN KEY(job_type_id)
	REFERENCES dw.job_type(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
GRANT SELECT(updated), INSERT(updated), UPDATE(updated), REFERENCES(updated) ON dw.batch TO diuser WITH GRANT OPTION
GO
GRANT SELECT(status_id), INSERT(status_id), UPDATE(status_id), REFERENCES(status_id) ON dw.batch TO diuser WITH GRANT OPTION
GO
GRANT SELECT(job_type_id), INSERT(job_type_id), UPDATE(job_type_id), REFERENCES(job_type_id) ON dw.batch TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.batch TO diuser WITH GRANT OPTION
GO
GRANT SELECT(created), INSERT(created), UPDATE(created), REFERENCES(created) ON dw.batch TO diuser WITH GRANT OPTION
GO
