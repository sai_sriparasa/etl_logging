CREATE TABLE dw.step  ( 
	id            	serial NOT NULL,
	status_id     	int4 NULL,
	created       	timestamp NULL DEFAULT now(),
	batch_id      	int4 NULL,
	step_type_id  	int4 NULL,
	updated       	timestamp NULL,
	lines_read    	int4 NULL,
	lines_written 	int4 NULL,
	lines_updated 	int4 NULL,
	lines_input   	int4 NULL,
	lines_output  	int4 NULL,
	lines_rejected	int4 NULL,
	step_name     	varchar(100) NULL,
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
ALTER TABLE dw.step
	ADD CONSTRAINT step-status
	FOREIGN KEY(status_id)
	REFERENCES dw.status(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
ALTER TABLE dw.step
	ADD CONSTRAINT step type
	FOREIGN KEY(step_type_id)
	REFERENCES dw.step_type(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
ALTER TABLE dw.step
	ADD CONSTRAINT batch-step
	FOREIGN KEY(batch_id)
	REFERENCES dw.batch(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
GRANT SELECT(updated), INSERT(updated), UPDATE(updated), REFERENCES(updated) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(step_type_id), INSERT(step_type_id), UPDATE(step_type_id), REFERENCES(step_type_id) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(step_name), INSERT(step_name), UPDATE(step_name), REFERENCES(step_name) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(status_id), INSERT(status_id), UPDATE(status_id), REFERENCES(status_id) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_written), INSERT(lines_written), UPDATE(lines_written), REFERENCES(lines_written) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_updated), INSERT(lines_updated), UPDATE(lines_updated), REFERENCES(lines_updated) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_rejected), INSERT(lines_rejected), UPDATE(lines_rejected), REFERENCES(lines_rejected) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_read), INSERT(lines_read), UPDATE(lines_read), REFERENCES(lines_read) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_output), INSERT(lines_output), UPDATE(lines_output), REFERENCES(lines_output) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_input), INSERT(lines_input), UPDATE(lines_input), REFERENCES(lines_input) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(created), INSERT(created), UPDATE(created), REFERENCES(created) ON dw.step TO diuser WITH GRANT OPTION
GO
GRANT SELECT(batch_id), INSERT(batch_id), UPDATE(batch_id), REFERENCES(batch_id) ON dw.step TO diuser WITH GRANT OPTION
GO
