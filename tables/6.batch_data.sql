CREATE TABLE dw.batch_data  ( 
	id        	serial NOT NULL,
	batch_id  	int4 NULL,
	datasource	varchar(4096) NULL,
	md5sum    	int4 NULL,
	created   	timestamp NOT NULL DEFAULT now(),
	PRIMARY KEY(id)
)
WITHOUT OIDS 
TABLESPACE pg_default
GO
ALTER TABLE dw.batch_data
	ADD CONSTRAINT batch - step data
	FOREIGN KEY(batch_id)
	REFERENCES dw.batch(id)
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
GO
COMMENT ON COLUMN dw.batch_data.datasource IS 'This column might hold URLs. Max length of URL Query is 4096'
GO
GRANT SELECT(md5sum), INSERT(md5sum), UPDATE(md5sum), REFERENCES(md5sum) ON dw.batch_data TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id), INSERT(id), UPDATE(id), REFERENCES(id) ON dw.batch_data TO diuser WITH GRANT OPTION
GO
GRANT SELECT(datasource), INSERT(datasource), UPDATE(datasource), REFERENCES(datasource) ON dw.batch_data TO diuser WITH GRANT OPTION
GO
GRANT SELECT(created), INSERT(created), UPDATE(created), REFERENCES(created) ON dw.batch_data TO diuser WITH GRANT OPTION
GO
GRANT SELECT(batch_id), INSERT(batch_id), UPDATE(batch_id), REFERENCES(batch_id) ON dw.batch_data TO diuser WITH GRANT OPTION
GO
