CREATE TABLE dw.stepid  ( 
	currval	int8 NULL 
	)
WITHOUT OIDS 
TABLESPACE pg_default
GO
GRANT SELECT(currval), INSERT(currval), UPDATE(currval), REFERENCES(currval) ON dw.stepid TO diuser WITH GRANT OPTION
GO
