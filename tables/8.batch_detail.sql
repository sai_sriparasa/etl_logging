CREATE TABLE dw.batch_detail  ( 
	id_job        	int4 NULL,
	channel_id    	varchar(255) NULL,
	jobname       	varchar(255) NULL,
	status        	varchar(15) NULL,
	lines_read    	int8 NULL,
	lines_written 	int8 NULL,
	lines_updated 	int8 NULL,
	lines_input   	int8 NULL,
	lines_output  	int8 NULL,
	lines_rejected	int8 NULL,
	errors        	int8 NULL,
	log_field     	text NULL,
	startdate     	timestamp NULL,
	enddate       	timestamp NULL,
	logdate       	timestamp NULL,
	depdate       	timestamp NULL,
	replaydate    	timestamp NULL 
	)
WITHOUT OIDS 
TABLESPACE pg_default
GO
CREATE INDEX idx_batch_detail_1
	ON dw.batch_detail USING btree (id_job int4_ops)
GO
CREATE INDEX idx_batch_detail_2
	ON dw.batch_detail USING btree (errors int8_ops, status text_ops, jobname text_ops)
GO
GRANT SELECT(status), INSERT(status), UPDATE(status), REFERENCES(status) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(startdate), INSERT(startdate), UPDATE(startdate), REFERENCES(startdate) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(replaydate), INSERT(replaydate), UPDATE(replaydate), REFERENCES(replaydate) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(logdate), INSERT(logdate), UPDATE(logdate), REFERENCES(logdate) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(log_field), INSERT(log_field), UPDATE(log_field), REFERENCES(log_field) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_written), INSERT(lines_written), UPDATE(lines_written), REFERENCES(lines_written) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_updated), INSERT(lines_updated), UPDATE(lines_updated), REFERENCES(lines_updated) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_rejected), INSERT(lines_rejected), UPDATE(lines_rejected), REFERENCES(lines_rejected) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_read), INSERT(lines_read), UPDATE(lines_read), REFERENCES(lines_read) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_output), INSERT(lines_output), UPDATE(lines_output), REFERENCES(lines_output) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_input), INSERT(lines_input), UPDATE(lines_input), REFERENCES(lines_input) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(jobname), INSERT(jobname), UPDATE(jobname), REFERENCES(jobname) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id_job), INSERT(id_job), UPDATE(id_job), REFERENCES(id_job) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(errors), INSERT(errors), UPDATE(errors), REFERENCES(errors) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(enddate), INSERT(enddate), UPDATE(enddate), REFERENCES(enddate) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(depdate), INSERT(depdate), UPDATE(depdate), REFERENCES(depdate) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(channel_id), INSERT(channel_id), UPDATE(channel_id), REFERENCES(channel_id) ON dw.batch_detail TO diuser WITH GRANT OPTION
GO
