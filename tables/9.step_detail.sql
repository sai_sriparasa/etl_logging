CREATE TABLE dw.step_detail  ( 
	id_batch       	int4 NULL,
	channel_id     	varchar(255) NULL,
	log_date       	timestamp NULL,
	transname      	varchar(255) NULL,
	stepname       	varchar(255) NULL,
	lines_read     	int8 NULL,
	lines_written  	int8 NULL,
	lines_updated  	int8 NULL,
	lines_input    	int8 NULL,
	lines_output   	int8 NULL,
	lines_rejected 	int8 NULL,
	errors         	int8 NULL,
	result         	bool NULL,
	nr_result_rows 	int8 NULL,
	nr_result_files	int8 NULL,
	log_field      	text NULL,
	copy_nr        	int4 NULL 
	)
WITHOUT OIDS 
TABLESPACE pg_default
GO
CREATE INDEX idx_step_1
	ON dw.step_detail USING btree (id_batch int4_ops)
GO
GRANT SELECT(transname), INSERT(transname), UPDATE(transname), REFERENCES(transname) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(stepname), INSERT(stepname), UPDATE(stepname), REFERENCES(stepname) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(result), INSERT(result), UPDATE(result), REFERENCES(result) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(nr_result_rows), INSERT(nr_result_rows), UPDATE(nr_result_rows), REFERENCES(nr_result_rows) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(nr_result_files), INSERT(nr_result_files), UPDATE(nr_result_files), REFERENCES(nr_result_files) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(log_field), INSERT(log_field), UPDATE(log_field), REFERENCES(log_field) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(log_date), INSERT(log_date), UPDATE(log_date), REFERENCES(log_date) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_written), INSERT(lines_written), UPDATE(lines_written), REFERENCES(lines_written) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_updated), INSERT(lines_updated), UPDATE(lines_updated), REFERENCES(lines_updated) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_rejected), INSERT(lines_rejected), UPDATE(lines_rejected), REFERENCES(lines_rejected) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_read), INSERT(lines_read), UPDATE(lines_read), REFERENCES(lines_read) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_output), INSERT(lines_output), UPDATE(lines_output), REFERENCES(lines_output) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(lines_input), INSERT(lines_input), UPDATE(lines_input), REFERENCES(lines_input) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(id_batch), INSERT(id_batch), UPDATE(id_batch), REFERENCES(id_batch) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(errors), INSERT(errors), UPDATE(errors), REFERENCES(errors) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(copy_nr), INSERT(copy_nr), UPDATE(copy_nr), REFERENCES(copy_nr) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
GRANT SELECT(channel_id), INSERT(channel_id), UPDATE(channel_id), REFERENCES(channel_id) ON dw.step_detail TO diuser WITH GRANT OPTION
GO
